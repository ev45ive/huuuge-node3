import { Router } from "express";
import { findUsers } from "../services/users.service";

export const userRoutes = Router()
// export const userRoutes1 = Router()

export const userOrders = Router()
userRoutes.use('/orders', userOrders)


const usersData = [
  { id: '123', name: 'Alice' },
  { id: '234', name: 'Bob' }
];

userRoutes.get('/', async (req, res) => {
  const queryFilter = String(req.query['filter'] || '')
  const result = await findUsers(queryFilter)

  res.json(result)
})

userRoutes.get('/me', (req, res) => {
  res.json(usersData[0])
})


userRoutes.get('/:user_id', (req, res) => {
  const user_id = String(req.params.user_id)

  if (!(Number(user_id) > 0)) {
    res.status(400).json({ message: `Invalid user_id "${user_id}"!` })
    return
  }

  const user = usersData.find(u => u.id === user_id);

  if (!user) {
    res.status(404).json({ message: `User with id: "${user_id}" not found!` })
    return
  }

  res.json(user)
})

// REST
// userRoutes.post('/tasks',(req,res)=>{}) // { type:'delete_users', id:'1234' }
// userRoutes.get('/tasks/1234',(req,res)=>{}) // { type:'delete_users', id:'1234', status:'pending' }
// userRoutes.put('/tasks/1234',(req,res)=>{}) // { type:'delete_users', id:'1234', status:'paused' }
// userRoutes.delete('/tasks/1234',(req,res)=>{}) // { type:'delete_users', id:'1234', status:'canceled' }


userRoutes.get('/:user_id/avatar.jpg', (req, res) => {
  // // force download of generated content
  // res.attachment('filename.csv')
  // res.send('csv,file,contents')

  // res.sendFile('filename')
  // res.download('path') // = res.attachment + res.sendFile

})