import { Router } from "express";
import fs from 'fs'
import { PassThrough } from "stream";
import { scanProductFiles } from "../lib/scanProductFiles";

export const catalogRouter = Router()

catalogRouter.get('/gen-prod-catalog', async (req, res) => {
  try {

    const rs1 = fs.createReadStream('./data/products/1270/description.html', {
      // start: req.range()...,
      highWaterMark: 50
    })
    const rs2 = fs.createReadStream('./data/products/1271/description.html', {
      highWaterMark: 50
    })

    const ws = fs.createWriteStream('./data/catalog.html');

    // rs1.on('data', data => ws.write(data))
    // rs1.on('end', (data: any) => ws.end(data))

    // Error [ERR_STREAM_WRITE_AFTER_END]: write after end
    // rs1.pipe(ws)
    // rs2.pipe(ws)

    // const streams = [rs1, rs2]
    // let pass = new PassThrough()
    // for (let stream of streams) {
    //     pass = stream.pipe(pass, { end: false })
    //     await new Promise((resolve, reject) => {
    //         stream.once('end', () => {
    //             streams.indexOf(stream) === streams.length - 1 && pass.end()
    //             resolve(null)
    //         })
    //         stream.once('error', reject)
    //     })
    // }
    // pass.on('end', (data: any) => res.send('ok'))
    // pass.pipe(ws)

    let pass = new PassThrough()
    pass.pipe(ws)

    for await (let file of scanProductFiles('./data/products')) {
      if (!file.includes('.html')) { continue; }
      console.log(file)

      const stream = fs.createReadStream(file);
      pass = stream.pipe(pass, { end: false })

      await new Promise((resolve, reject) => {
        stream.once('end', resolve)
        stream.once('error', reject)
      })
    }
    pass.on('end', (data: any) => res.send('ok'))
    pass.end()


  } catch (err) {
    res.send(err)
  }

})

// catalogRouter.get('/gen-prod-catalog', async (req, res) => {
//     try {

//         const rs = fs.createReadStream('./data/in.txt', {
//             highWaterMark: 11, // Buffer size
//             // encoding:'ascii'
//         })
//         const ws = fs.createWriteStream('./data/out.txt');

//         rs.on('end', () => { res.end('ok') })
//         rs.on('error', data => res.write(data))
//         rs.pipe(ws)

//         rs.pipe(res)
//         rs.pipe(process.stdout)

//         // console.clear()

//         // ws.write(rs.read(10), () => {
//         //     rs.on('readable', () => { /* ... */ })
//         // })
//         // rs.on('data', data => console.log(data))
//         // rs.on('data', data => ws.write(data))
//         // rs.on('end', (data: Buffer | null) => ws.end(data))

//         // rs.on('error', data => console.error(data))

//     } catch (err) {
//         res.send(err)
//     }

// })

// catalogRouter.get('/gen-prod-catalog', async (req, res) => {
//     try {

//         const buf = Buffer.alloc(10, '')

//         const fd1 = await fs.promises.open('./data/in.txt', 'r+')
//         const fd2 = await fs.promises.open('./data/out.txt', 'w+');

//         let pos = 0
//         do {
//             var { bytesRead } = await fd1.read(buf, 0, buf.length, pos)
//             await fd2.write(buf, 0, bytesRead, pos)
//             console.log(pos, bytesRead, buf.toString())
//             pos += bytesRead
//         } while (bytesRead)

//         await fd1.close()
//         await fd2.close()

//         res.send('ok')
//     } catch (err) {
//         console.error(err)
//         res.send(err)
//     }

// })