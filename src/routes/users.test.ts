import supertest from 'supertest'
import { app } from '../app'
import { mocked } from 'ts-jest/utils'
import { findUsers, User } from '../services/users.service'

jest.mock('../services/users.service')

describe('Users Routes', () => {
  test('Truth is truthy', () => {
    expect(true).toEqual(true)
  })

  test('GET /users returns all users', async () => {
    const usersMock = [
      { id: '123', name: 'Alice' },
      { id: '234', name: 'Bob' }
    ]
    const request = supertest(app)

    const mock = mocked(findUsers)
    mock.mockResolvedValue(usersMock)

    await request.get('/users')
      .expect(200, usersMock)

    expect(mock).toHaveBeenCalledWith('')
  })

  test('GET /users returns filtered users', async () => {
    const request = supertest(app)

    const mock = mocked(findUsers)

    mock.mockReset()
    mock.mockResolvedValue([{ id: '234', name: 'Bob' }])
    await request.get('/users?filter=Bob')
      .expect(200, [{ id: '234', name: 'Bob' }])
    expect(mock).toHaveBeenCalledWith('Bob')

    mock.mockReset()
    mock.mockResolvedValue([{ id: '123', name: 'Alice' }])
    await request.get('/users?filter=A')
      .expect(200, [{ id: '123', name: 'Alice' }])
    expect(mock).toHaveBeenCalledWith('A')
  })
})
