import { /* Router, */ Request, Response, NextFunction, RequestHandler } from "express";
import { createProduct, findProducts, getProductById, initProducts } from "../services/products.service";
import Router from 'express-promise-router'
import { NotFoundException, ValidateProductDTO, ValidationError } from "../common/errors";

export const productRoutes = Router()

productRoutes.get('/', async (req, res) => {
  res.json(await findProducts())
})

productRoutes.get('/init', async (req, res) => {
  await initProducts()
  res.json({ done: true })
})

productRoutes.get('/track_visits', (req, res) => {
  req.session.visits = req.session.visits || 0
  req.session.visits++

  res.json({
    visits: req.session.visits
  })

})

productRoutes.get('/:product_id', async (req, res) => {
  const product_id = req.params['product_id'];

  if (!product_id) { throw new NotFoundException() }
  const result = await getProductById(product_id);
  res.json(result)
})

productRoutes.post('/',
  ValidateProductDTO(),
  async (req, res) => {
    const dto = req.body
    const result = await createProduct(dto);

    res.status(201).json(result)
  })


// node_modules\@types\express-session\index.d.ts
declare module 'express-session' {
  interface SessionData {
    visits?: number;
  }
}