import supertest from "supertest"
import { mocked } from "ts-jest/utils"
import { app } from "../app"
import { createProduct, findProducts, getProductById, Product, ProductCreateDTO } from "../services/products.service"


jest.mock('../services/products.service')

describe('Products Routes', () => {
  let request: supertest.SuperTest<supertest.Test>

  beforeEach(() => {
    request = supertest(app)
  })

  // it.only('Gets all products', async () => {
  it('Gets all products', async () => {
    const mockProducts = [
      { id: '123', name: 'Product 123' },
      { id: '234', name: 'Product 234' },
    ]
    mocked(findProducts).mockResolvedValue(mockProducts)
    await request.get('/products')
      .expect(200, mockProducts)

    expect(mocked(findProducts)).toHaveBeenCalledWith()
  })

  it('Gets product by id', async () => {
    const mockProduct = { id: '123', name: 'Product 123' }
    mocked(getProductById).mockResolvedValue(mockProduct)

    await request.get('/products/123')
      .expect(200, mockProduct)
    expect(mocked(getProductById))
      // .toHaveBeenCalledWith<Parameters<typeof getProductById>>('123')
      .toHaveBeenCalledWith<[Product['id']]>('123')
  })

  it('Creates new product', async () => {
    mocked(createProduct).mockResolvedValue({
      id: '234', name: 'New Product'
    })
    await request.post('/products')
      .send({ name: 'New Product' })
      .expect(201, {
        id: '234',
        name: 'New Product'
      })
    expect(mocked(createProduct))
      .toHaveBeenCalledWith<[ProductCreateDTO]>({
        name: 'New Product'
      })
  })

  it.todo('Updates new product')
  it.todo('Removes new product')

  it('tracks product visits', async () => {
    await request.get('/products/123')
          

  })

})
