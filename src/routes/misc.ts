import { Express, Router } from 'express'
import path from 'path'
import fs from 'fs'
import { generateDescriptionHTML } from '../lib/generateDescriptionHTML'
import { generateDescriptionHTMLPromise } from '../lib/generateDescriptionHTMLPromise'
import { generateLogsGenerator } from '../lib/log-gen-generator'
import ProductUpdateService from '../lib/productUpdateService'
import { scanProductFiles } from '../lib/scanProductFiles'

export const miscRoutes = Router()

ProductUpdateService.on('product-update', (productData) => {
  // Log
  console.log('Product updated ', JSON.stringify(productData, null, 2))
})
ProductUpdateService.on('product-update', (productData) => {
  // Send email notification
  fs.promises.writeFile('./data/out/updated-product.json', JSON.stringify(productData, null, 2))
})


miscRoutes.get('/product-update', async (req, res) => {
  // ProductUpdateService.on('product-update', (productData) => {
  //     res.send(productData)
  // })
  // app.on()
  // app.emit()
  // req.app.emit()
  // req.app.on()
  // req.on('close')

  res.send(await ProductUpdateService.updateProduct())
})

miscRoutes.get('/gen-desc', (req, res) => {
  const product_id = 1270

  generateDescriptionHTML(product_id, (err, outPath) => {
    if (err) {
      return res.send(err);
    }
    res.send(`Created file ${outPath} for product ${product_id}`);
  });
})


miscRoutes.get('/gen-desc-promise', async (req, res) => {
  try {
    const product_id = 1270
    const outPath = await generateDescriptionHTMLPromise(product_id);

    res.send(`Created file ${outPath} for product ${product_id}`)

  } catch (error) {
    res.send(error)
  }
})

miscRoutes.get('/products-files', async (req, res) => {
  try {
    // send list of products files
    const dirPath = path.resolve(`./data/products/`);
    const paths = await scanProductFiles(dirPath);
    const pattern = '.md'
    const limit = 10;

    const results = []
    let i = 0;
    for await (let file of paths) {
      if (!(pattern && file.includes(pattern))) { continue }
      if (limit && i++ > limit) { break }

      results.push(path.relative(dirPath, file))
    }

    // const result = paths
    //     .map(file => path.relative(dirPath, file))
    //     .filter(file => file.includes(pattern))
    //     .slice(0,10)


    res.json(results)
  } catch (error) {
    res.json(error)
  }
})

miscRoutes.get('/time', (req, res) => {
  // throw new Error('error')
  res.send('<h1>Hello ' + (new Date().toLocaleTimeString()) + '</h1>')
})

miscRoutes.get('/async', (req, res) => {
  // throw new Error('error')
  res.set('Content-Type', 'text/html')
  res.flushHeaders()

  res.write('<p>non-blocking</p>')
  let i = 0;
  const handle = setInterval(() => {
    res.write('<p>non-blocking even more...</p>')
    if (i++ > 5) {
      clearInterval(handle)
      res.end('Done non-blocking! ;-)')
    }
  }, 1000)

  // Error [ERR_STREAM_WRITE_AFTER_END]: write after end
  // res.end()
})

miscRoutes.get('/sync', (req, res) => {
  // throw new Error('error')
  res.set('Content-Type', 'text/html')
  res.flushHeaders()

  for (let i = 0; i < 5; i++) {
    res.write('<p>blocking</p>')

    // Block for 1 second
    const future = Date.now() + 1000
    while (future > Date.now()) { }
  }
  res.end()
})


miscRoutes.get('/blocking', (req, res) => {
  // throw new Error('error')
  res.send('<h1>Hello </h1> <input style="width:100%; height:50px;" id="textfield" autofocus>')
})

miscRoutes.get('/gen-logs', async (req, res) => {
  let closed = false;
  req.on('close', () => {
    closed = true;
    console.log('Disconnected!')
  })

  try {
    const logNum = Number(req.query.n) || 100
    const absPath = (path.resolve(__dirname, '../data/log.txt'))

    var i = 0;
    for await (let log of generateLogsGenerator(absPath)) {
      if (i++ >= logNum) { break; }
      if (closed) { break; }
    }
    res.send('<h1>Hello Logs </h1><p> Written ' + i + ' logs')

  } catch (error) {
    res.send('<h3>Error ' + error.message + '</h3>')
  }
})
