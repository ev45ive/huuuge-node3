import { exit } from "yargs";

console.clear()
// https://nodejs.org/dist/latest-v14.x/docs/api/globals.html
// https://nodejs.org/dist/latest-v14.x/docs/api/process.html#process_process

console.log('============ Info Task ==============');

var x = 123;
// equivalent of browser 'window' - top level scope
(global as any).x === 123;


process // current system process information
console.log('============ Process ==============');

console.log('process.pid', process.pid)
console.log('process.platform', process.platform)
console.log('process.ppid', process.ppid)
console.log('process.arch', process.arch)
console.log('process.argv0', process.argv0)
console.log('process.argv', process.argv)
console.log('process.execArgv', process.execArgv)

console.log('============ Params ==============');
console.log('params', process.argv.slice(2))

console.log('============ ENV ==============');
// export PORT=6000 && ts-node ./src/tasks/info-task.ts
// set PORT=8000 && ts-node .\src\tasks\info-task.ts
// set PORT=9000 && ts-node .\\src\\tasks\\info-task.ts

// cross-env PORT=6000 ts-node ./src/tasks/info-task.ts

console.log('process.env.PORT', process.env.PORT)
console.log('process.env.HOST', process.env.HOST)

console.log('============ Paths ==============');
// __dir__
// __file__
console.log('__dirname', __dirname) // current directory
console.log('__filename', __filename) // current file name
console.log('process.cwd()', process.cwd())
process.chdir('..')
console.log('process.chdir(../)', process.cwd())
// process.chdir(__dirname)

console.log('============ Resources ==============');
console.log('process.cpuUsage()', process.cpuUsage())
console.log('process.resourceUsage()', process.resourceUsage())
console.log('process.memoryUsage()', process.memoryUsage())

console.log('============ Resources ==============');
process.on('beforeExit', console.log)
console.log('process.version', process.version)
console.log('process.versions', process.versions)
console.log('process.uptime()', process.uptime())

// process.kill(1234, 'SIGUSR_1')
// process.send('')

process.stdin
process.stdout
process.stderr

/* Async Timer Functions */
setTimeout
setInterval
setImmediate
process.nextTick

console.log('============ === ==============');
// nodemon -w 'src/tasks/**' -e 'ts' --exec 'ts-node src/tasks/info-task.ts --param=123 pathAbc'

import readline from 'readline';
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    // completer(linePartial, callback) { callback(null, [['123'], linePartial]); })
});
rl.on('line', (line: string) => { 
    console.log(`Received: ${line}`);
    process.exit(0)
 });
rl.prompt();

// ts-node  src/tasks/info-task.ts && echo 'Placki'
// process.exit(0) // inny niz 0 - nie wykona kolejnego polecenia
