import { generateLogs } from "../lib/log-gen";
import yargs, { Argv } from 'yargs'


// ts-node ./src/tasks/log-gen.ts -n 1000000 ./data/log.txt && echo 'DONE!'

// https://nodejs.org/dist/latest-v14.x/docs/api/path.html
import path from 'path'

// https://nodejs.org/dist/latest-v14.x/docs/api/fs.html
import fs from 'fs'
import { generateLogsSync } from "../lib/log-gen-sync";

// ts-node src/tasks/log-gen.ts -n 10 ./data/log.txt
// nodemon --exec 'node -r ts-node/register src/tasks/log-gen.ts -n 10 ./data/log.txt '

const args = yargs(process.argv.splice(2))
// console.log(args.argv)
const { _: [logPath], n: logNum } = args.argv as unknown as { _: [string], n: number }
console.clear();

// console.log(process.cwd())
// console.log(logPath)
// console.log(process.cwd() + logPath)

const absPath = path.resolve(process.cwd(), logPath)
const dirPath = path.dirname(absPath)

if (!fs.existsSync(dirPath)) {
    fs.mkdirSync(dirPath, { recursive: true })
}

// console.log(dirPath);
const result = generateLogsSync(absPath,logNum)
console.log('Written ' + result + ' logs to file ' + absPath)

// generateLogs(absPath, logNum, (result:number) => {
//     console.log('Written ' + result + ' logs to file ' + absPath)
// })


