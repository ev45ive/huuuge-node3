import { createUser, findUsers, getUserById, User } from "./users.service"


const mockUsers = [
  { id: '123', name: 'Alice' },
  { id: '234', name: 'Bob' }
]

describe('Users Service', () => {

  // test('is created', () => {})

  it('finds users', async () => {

    const result = await findUsers('')

    expect(result).toEqual(mockUsers)
  })

  it('gets user by id', async () => {
    await expect(getUserById('123')).resolves
      .toEqual<Partial<User>>({ id: '123', name: 'Alice' })

    expect(await getUserById('234'))
      .toEqual<Partial<User>>({ id: '234', name: 'Bob' })
  })

  test('should create user', async () => {
    const result = await createUser({
      name: 'NewUser'
    })
    expect(result).toEqual(expect.objectContaining<Partial<User>>({
      id: expect.any(String),
      name: 'NewUser'
    }))
    await expect(getUserById(result.id))
      .resolves
      .toEqual(expect.objectContaining<Partial<User>>({
        id: result.id,
        name: 'NewUser'
      }))
  })

  it.todo('updates user')
  it.todo('deletes user')

})


