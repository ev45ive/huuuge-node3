import { Product } from "../model/product"

export interface User {
  id: string
  name: string,
  // createdAt: string
}

const users: User[] = [
  { id: '123', name: 'Alice' },
  { id: '234', name: 'Bob' }
]

export const findUsers = async (filter: string) => {
  return users
}

export const getUserById = async (id: string) => {
  // return { id: '123', name: 'Alice' }
  return users.find(user => user.id === id)
}

export const createUser = async (data: { name: string }): Promise<User> => {

  const newUser: User = {
    id: Date.now().toString(),
    name: data.name,
    // createdAt: (new Date()).toString() // Error - not user
  }
  users.push(newUser)
  return newUser
}

