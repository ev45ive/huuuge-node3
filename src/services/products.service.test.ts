import { mocked } from "ts-jest/utils"
import { Product } from "../model/product"
import { findProducts } from './products.service'

jest.mock("../model/product")

describe('Products Service', () => {

  it('Gets all products', async () => {
    mocked(Product).find.mockResolvedValue([1, 2, 3])
    
    await expect(findProducts()).resolves.toEqual([1, 2, 3])
  })

  it.todo('Gets product by id')
  it.todo('Creates new product')
  it.todo('Updates new product')
  it.todo('Removes new product')

})
