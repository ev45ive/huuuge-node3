import { Product } from "../model/product"

export interface Product {
  id: string
  name: string
}
// type ProductCreateDTO = {
//   name: string
// }

const products: Product[] = []

export type ProductCreateDTO = Partial<Pick<Product, 'name'>>

export async function findProducts(filter?: string): Promise<Product[]> {
  return Product.find({})
}

export async function getProductById(id: Product['id']): Promise<Product> {
  return Product.findById(id)
}

export async function createProduct(payload: ProductCreateDTO): Promise<Product> {
  const prod = await Product.create(payload)
  return prod
}

export const initProducts = async () => {
  await Product.insertMany([
    { name: 'Product 123', price: 123 },
    { name: 'Product 234', price: 234 },
    { name: 'Product 345', price: 345 },
  ])

}