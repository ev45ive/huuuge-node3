import express, { ErrorRequestHandler } from 'express';
import { miscRoutes } from './routes/misc';
import { catalogRouter } from './routes/catalog';
import { userRoutes } from './routes/users';
import { productRoutes } from './routes/products';
import { Response } from 'express'
import session from 'express-session';

export const app = express();

app.use(session({
  secret:'secret cat',
  resave:true,
  saveUninitialized:true,
}))


app.use(express.urlencoded({ extended: true }))
app.use(express.json({/* ... */ }))
app.use('/public', express.static('./public/'))

app.use('/misc', miscRoutes);
app.use('/catalog', catalogRouter);
app.use('/users', userRoutes);
app.use('/products', productRoutes);

app.get('/', (req, res) => {
  res.send('<h1>Hello Express + TS-node + nodemon!</h1>');
});

app.use(((error, req, res, next) => {
  res.status(error.status || 500).json({
    message: error.message
  })
  // next(error)
}) as ErrorRequestHandler)



// let currentReq: Response

// app.use((req, res, next) => {
//     currentReq = res;
//     next()
// })

// process.on('unhandledRejection', (rejection) => {
//     currentReq.json({
//         error: { message: rejection }
//     })
// })