
import fs from 'fs'
import path from 'path'

export async function generateLogsAsyncAwait(logPath: string, n = 1000/* cancelToken */) {
    const absPath = path.resolve(process.cwd(), logPath)
    const dirPath = path.dirname(absPath)

    await fs.promises.mkdir(dirPath, { recursive: true })

    for (var i = 0; i < n; i++) {
        const log = `[ ${(new Date()).toISOString()} ] [ Info ] Example log - nonce ${Math.floor(Math.random() * 100_000)} \n`

        // if(cancelToken.cancelled){ throw new Error('Cancelled!')}

        await fs.promises.appendFile(logPath, log)
    }

    return i;
}