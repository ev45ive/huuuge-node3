
import fs from 'fs'
import path from 'path'

export function generateLogsPromise(logPath: string, n = 1000) {
    const absPath = path.resolve(process.cwd(), logPath)
    const dirPath = path.dirname(absPath)

    let i = 0;

    return fs.promises.mkdir(dirPath, { recursive: true }).then(() => {
        return generateNextLog(logPath)
    })

    function generateNextLog(logPath: string): Promise<number> {
        const log = `[ ${(new Date()).toISOString()} ] [ Info ] Example log - nonce ${Math.floor(Math.random() * 100_000)} \n`

        return fs.promises.appendFile(logPath, log)
            .then(() => {
                if (i++ > n) { return (n) }
                // return Promise.reject(new Error('fake error!'))
                return generateNextLog(logPath)
            })
    }
}