
import fs from 'fs'
import path from 'path'


export function generateLogs(logPath: string, n = 1000, callback: any) {

    const absPath = path.resolve(process.cwd(), logPath)
    const dirPath = path.dirname(absPath)

    let i = 0;

    fs.mkdir(dirPath, { recursive: true }, (error) => {
        if (error) { return callback(error) }

        generateNextLog(logPath)
    })


    function generateNextLog(logPath: string) {
        const log = `[ ${(new Date()).toISOString()} ] [ Info ] Example log - nonce ${Math.floor(Math.random() * 100_000)} \n`

        fs.appendFile(logPath, log, (error) => {
            if (error) {
                return callback(error)
            }

            if (i++ > n) {
                callback(null, n)
                return
            }

            generateNextLog(logPath)
        })
    }



    // let i = 0
    // const handle = setInterval(() => {
    //     if (i++ > n) { handle }
    //     dateBuff.fill('').write((new Date()).toISOString())
    //     typeBuff.fill('').write(['Info', 'Error', 'Warning'][Math.floor(Math.random() * 3)])
    //     messageBuff.fill('').write(`Example log with request_id ${Math.floor(Math.random() * 1000)}-${Math.floor(Math.random() * 1000)}`)

    //     fs.appendFileSync(logPath, logBuff)
    // }, 10)

    // fs.writeFileSync(logPath, result)

}