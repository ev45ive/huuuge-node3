import path from 'path';
import fs from 'fs';

export async function* scanProductFiles(dirPath: string/* , pattern?: string */): AsyncGenerator<string> {
    
    // const files = await fs.promises.readdir(dirPath);
    const dir = await fs.promises.opendir(dirPath)

    for await (let file of dir) {
        const filePath = path.join(dirPath, file.name)
        const stat = await fs.promises.stat(filePath)
        if (stat.isDirectory()) {
            // const res = await scanProductFiles(filePath)
            // for await (let r of res) {
            //     yield r
            // }
            // OR:
            yield* scanProductFiles(filePath)
        } else {
            yield filePath
        }
    }
}

export async function scanProductFiles2(dirPath: string/* , pattern?: string */) {
    const dirs = await fs.promises.readdir(dirPath);

    const paths = [];

    for (let dir of dirs) {
        const subDirPath = path.resolve(dirPath, dir);
        const files = await fs.promises.readdir(subDirPath);

        for (let file of files) {
            // if (!(pattern && file.includes(pattern))) { continue }
            // if ('function' == typeof mapFn) { file = mapFn(file) }
            // if (limit && i > limit) { break }

            paths.push(path.join(subDirPath, file))
        }

    }
    return paths;
}
