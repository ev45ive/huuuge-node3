import path from 'path';
import fs from 'fs';
import { Converter } from 'showdown';

export async function generateDescriptionHTMLPromise(product_id: number): Promise<string> {
  const converter = new Converter();
  const inPath = path.resolve(`./data/products/${product_id}/description.md`);
  const outPath = path.resolve(`./data/out/products/${product_id}/description.html`);
  const outDir = path.dirname(outPath);

  try {
    await fs.promises.stat(outDir);
  } catch (fileDoesNotExists) {
    const data = await fs.promises.readFile(inPath)
    const markdown = converter.makeHtml(data.toString())

    await fs.promises.mkdir(outDir)
    await fs.promises.writeFile(outPath, markdown)
  } finally {
    return outPath
  }
}


export function generateDescriptionHTMLPromise3(product_id: number): Promise<string> {
  const converter = new Converter();
  const inPath = path.resolve(`./data/products/${product_id}/description.md`);
  const outPath = path.resolve(`./data/out/products/${product_id}/description.html`);
  const outDir = path.dirname(outPath);

  return fs.promises.stat(outDir)
    .then(() => outPath)
    .catch((fileDoesNotExists) => fs.promises.readFile(inPath)
      .then(data => converter.makeHtml(data.toString()))
      .then(markdown => fs.promises.mkdir(outDir).then(() => markdown))
      .then((markdown) => fs.promises.writeFile(outPath, markdown))
      .then(() => outPath)
    )
  // .finally(() => outPath) // Just void callback - return value Not included in promise
}


export function generateDescriptionHTMLPromise2(product_id: number) {
  const converter = new Converter();
  const inPath = path.resolve(`./data/products/${product_id}/description.md`);
  const outPath = path.resolve(`./data/out/products/${product_id}/description.html`);
  const outDir = path.dirname(outPath);

  return fs.promises.stat(outDir).then(() => {
    return outPath
  }, (fileDoesNotExists) => {
    // open /data/products/${id}/description.md
    return fs.promises.readFile(inPath).then(data => {

      // https://www.npmjs.com/package/showdown
      // convert markdown 
      const markdown = converter.makeHtml(data.toString())

      return fs.promises.mkdir(outDir)
        .then(() => {
          // save /data/out/products/${id}/description.html
          return fs.promises.writeFile(outPath, markdown).then(() => {
            return outPath
          })
        })
    })

  })
}
