import EventEmitter from "events";

// const emitter = new EventEmitter({})

export class ProductUpdateService extends EventEmitter {

  async updateProduct() {
    const updated = {
      id: 1270,
      description: 'Updated Description',
      updatedAt: (new Date()).toJSON()
    };
    this.emit('product-update', updated)

    return updated
  }

}

export default new ProductUpdateService()