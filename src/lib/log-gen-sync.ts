
import fs from 'fs'

function slicePattern(buffer: Buffer, pattern: Buffer | string) {
    const startPos = buffer.indexOf(pattern)
    const endPos = startPos + pattern.length;

    return buffer.slice(startPos, endPos)
}

export function generateLogsSync(logPath: string, n = 1000) {

    // const log = `[ ${(new Date()).toISOString()} ] [ Info ] Example log - nonce ${Math.floor(Math.random() * 100_000)} \n`
    // const logBuff = Buffer.alloc(log.length)
    const datePattern = '0000-00-00T00:00:00.000Z'
    const typePattern = 'Warning'
    const messagePattern = Array(30).join('X')
    const logBuff = Buffer.from(`[ ${datePattern} ] [ ${typePattern} ] ${messagePattern} \n`)

    const dateBuff = slicePattern(logBuff, datePattern)
    const typeBuff = slicePattern(logBuff, typePattern)
    const messageBuff = slicePattern(logBuff, messagePattern)

    for (let i = 0; i < n; i++) {
        dateBuff.fill('').write((new Date()).toISOString())
        typeBuff.fill('').write(['Info', 'Error', 'Warning'][Math.floor(Math.random() * 3)])
        messageBuff.fill('').write(`Example log with request_id ${Math.floor(Math.random() * 1000)}-${Math.floor(Math.random() * 1000)}`)

        fs.appendFileSync(logPath,logBuff)
    }

    // fs.writeFileSync(logPath, result)

    return n
}