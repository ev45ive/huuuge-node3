import { Response } from 'express';
import fs from 'fs';
import path from 'path';
import { Converter } from 'showdown';

export function generateDescriptionHTML(
    product_id: number,
    callback: (err: any, result?: any) => void
) {
    const converter = new Converter();
    const inPath = path.resolve(`./data/products/${product_id}/description.md`);
    const outPath = path.resolve(`./data/out/products/${product_id}/description.html`);
    const outDir = path.dirname(outPath);

    // if output file aleady exists?
    fs.stat(outDir, (err) => {
        if (!err) { return callback(null, outPath) }

        // open /data/products/${id}/description.md
        fs.readFile(inPath, (err, data) => {
            if (err) { return callback(err); }

            // convert markdown // https://www.npmjs.com/package/showdown
            try {
                var markdown = converter.makeHtml(data.toString());
            } catch (error) { return callback(error) }

            // create directory
            fs.mkdir(outDir, { recursive: true }, (err, path) => {
                if (err) { return callback(err); }

                // save /data/out/products/${id}/description.html
                fs.writeFile(outPath, markdown, (err) => {
                    if (err) { return callback(err); }
                    // show html to user
                    callback(null, outPath)
                });
            });
        });
    })

  /*   if (fs.statSync(outPath)) { // FIXME: convert to Async, remove nextTick
        process.nextTick(() => { callback(null, outPath) })
        return;
    }

    // open /data/products/${id}/description.md
    fs.readFile(inPath, (err, data) => {
        if (err) { return callback(err); }

        // convert markdown // https://www.npmjs.com/package/showdown
        try {
            var markdown = converter.makeHtml(data.toString());
        } catch (error) { return callback(error) }

        // create directory
        fs.mkdir(outDir, { recursive: true }, (err, path) => {
            if (err) { return callback(err); }

            // save /data/out/products/${id}/description.html
            fs.writeFile(outPath, markdown, (err) => {
                if (err) { return callback(err); }
                // show html to user
                callback(null, outPath)
            });
        });
    }); */

}
