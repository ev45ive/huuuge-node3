
import fs from 'fs'
import path from 'path'

export async function* generateLogsGenerator(logPath: string) {
    const absPath = path.resolve(process.cwd(), logPath)
    const dirPath = path.dirname(absPath)

    await fs.promises.mkdir(dirPath, { recursive: true })

    while (true) {
        const log = `[ ${(new Date()).toISOString()} ] [ Info ] Example log - nonce ${Math.floor(Math.random() * 100_000)} \n`

        await fs.promises.appendFile(logPath, log)

        yield log
    }
}