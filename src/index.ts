
import path from 'path';
import { app } from './app';
import mongoose from 'mongoose'
import { Server } from 'http';

// Set working dir for all FS operations to this directory
process.chdir(path.resolve(__dirname, '..')) // or always npm run ...

const hostname = process.env.HOST || '127.0.0.1'
const port = Number(process.env.PORT) || 8080

let server: Server

mongoose.connection.once("open", () => {
    server = app.listen(port, hostname, () => {
        console.log(`Server running at http://${hostname}:${port}/`)
    })
});


mongoose.connect("mongodb://localhost:27017/database", {
    useNewUrlParser: true,
    autoIndex: process.env.NODE_ENV === "development"
});

// server.on('close', () => { })
process.on('beforeExit', async () => {
    console.log('Closing...')
    await new Promise(resolve => server.close(resolve))
    await mongoose.disconnect()
    process.exit()
})
