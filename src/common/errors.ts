import { RequestHandler } from "express";

export class NotFoundException extends Error {
  status = 404
  constructor(public message = 'Not Found') {
    super();
  }
}
export class ValidationError extends Error {
  status = 400
  constructor(public message = 'Validation Failed') {
    super();
  }
}

export function ValidateProductDTO(): RequestHandler {
  return (req, res, next) => {
    const data = req.body

    if (!('name' in data)) { return next(new ValidationError()) }
    // if (!('placki' in data)) { throw (new ValidationError()) }

    next()
  }
}