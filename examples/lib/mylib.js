import { strictEqual } from 'assert';

var secret = 'secret sauce';

console.log(secret);

export function assertDessert (dessert) {
    strictEqual(dessert, 'placki', 'Only Placki')
}

export function getPlacki () {
    return '<h3>Lubie placki</h3>'
}

export function getTitle () {
    return '<h1>NodeJS App</h1>'
}

export default getPlacki