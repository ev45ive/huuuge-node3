```js
function idMaker(max = 5) {
     let index = 0;
     return {
         next: function() {
             done = index >= max;
                
             if(!done){ index++ }

             return {
                 value: index,
                 done: done
             };
         }
     };
}
let it = idMaker();
// console.log(it.next());

// while((res = it.next()) && res.done !== true){
//     console.log(res)
// }
```

## Symbol.iterator

```js
function idMaker(max = 5) {
     let index = 0;
     return {
         [Symbol.iterator]: function(){
            return {
                next:function() {
                     done = index >= max;

                     if(!done){ index++ }

                     return {
                         value: index,
                         done: done
                     };
                 }
             }
         }
     };
}
let it = idMaker();

for(let res of it){
    console.log(res)
}
```

## ASync iterator

```js
delay = (value) => new Promise(resolve => setTimeout(() => resolve(value), 1000))

function idMaker(max = 5) {
     let index = 0;
     return {
         [Symbol.asyncIterator](){
            return {
                next: function() {
                     done = index >= max;

                     if(!done){ index++ }

                     return delay({
                         value: index,
                         done: done
                     })
                 }
             }
         }
     };
}

;(async ()=>{
    let it = idMaker();

    for await(let res of it){
        if(res % 2 == 0){ continue; }
        if(res > 3){ break; }
        console.log(res)
    }

})()
```

## Generator
```js
gen = function*(){
    yield 1;
    const msg = yield 2;
    console.log('recieved '+msg)
    return 3;
}
iter = gen()
console.log(iter.next())
console.log(iter.next())
console.log(iter.next('hello!'))
console.log(iter.next())

{value: 1, done: false}
{value: 2, done: false}
recieved hello!
{value: 3, done: true}
{value: undefined, done: true}
```

```js
delay = (value) => new Promise(resolve => setTimeout(() => resolve(value), 1000))

function idMaker(max = 5) {
     return {
         [Symbol.asyncIterator]: async function*(){
            for(let i = 0; i<max; i++){
                yield delay(i)
            }
         }
     };
}

;(async ()=>{
    let it = idMaker();

    for await(let res of it){
        if(res % 2 == 0){ continue; }
        if(res > 3){ break; }
        console.log(res)
    }
    console.log('complete')
})()
```