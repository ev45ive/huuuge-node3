```js
function echo(msg, cb){
    setTimeout(()=>{
        Math.random() < 0.5? cb(null,msg) : cb('ups..',null)
    },2000)
}

echo('Alice', (error, result) => {
    error? console.error(error) : console.log(result);
})
```

## Callback hell
```js
function echo(msg, cb){
    setTimeout(()=>{
        Math.random() < 1? cb(null,msg) : cb('ups..',null)
    },2000)
}

echo('Alice', (error, result) => {
    if(error) { console.error(error); return;  }

    echo(result + ' ma ', (error, result) => {
        if(error) { console.error(error); return;  }

        echo(result + ' kota', (error, result) => {
            if(error) { console.error(error); return;  }

            console.log(result)
        })
    })
})
```

## Promise
```js
function echo(msg){
    return new Promise((resolve, reject)=>{

        setTimeout(()=>{
            Math.random() < 1? resolve(msg) : reject('ups..')
        },2000)
    })
}

promise = echo('Alice')

// === 
// Wait in 2 places:

promise.then(result => { console.log(result) })
promise.then(console.log)

// Promise remembers result
setTimeout(() => promise.then(console.log), 3000)

```

## Promise chain
```js

p = echo('Alice')

p2A = p.then( res => res + ' ma kota synchronicznego')

p2B = p.then( res => echo(res + ' ma kota asynchronicznego') )

p3A = p2A.then( res => echo(res + ' i placki'))
p3B = p2B.then( res => echo(res + ' i placki'))

p3A.then(console.log)
p3B.then(console.log)

```

## Promise error handling

```js
function echo(msg, err){
    return new Promise((resolve, reject)=>{

        setTimeout(()=>{
            (!err)? resolve(msg) : reject(err)
        },2000)
    })
}

// Promise.resolve()
// Promise.reject()

p = echo('Alice','ups..')
    .catch(() => 'Anonim ')

// one sync one async
p2A = p.then( res => res + ' ma kota synchronicznego', () => 'Nie ma Nikogo ')
p2B = p.then( res => echo(res + ' ma kota asynchronicznego'), () => echo('Też Nie ma Nikogo ') )

// one finishes one fails
p3A = p2A.then( res => echo(res + ' i placki', 'ouch...'))
p3B = p2B.then( res => echo(res + ' i placki'))

// each finish or fails separately
p3A.then(console.log).catch(console.error)
p3B.then(console.log).catch(console.error)

// All finish or none finish (if one fails)
Promise.all([
    p3A,
    p3B
])
.then(console.log)
.catch(console.error)
.finally(res => ' i koniec!')
​
```

## Recursive promise

```js
function echo(msg, err){
    return new Promise((resolve, reject)=>{

        setTimeout(()=>{
            (!err)? resolve(msg) : reject(err)
        },500)
    })
}

i = 0 
max = 5;

function nextPromise(fn, n){
    if(n == 0){ return; }

    fn(n).then((i) => {
        console.log(i)
        nextPromise(fn, n-1)
    })
}
nextPromise(echo, max)
```