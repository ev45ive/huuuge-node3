```js
textfield.onkeydown = () => console.log('0 Keyboard event')

console.log('1 - log 1 before timeout')

// setTimeout( callback  ,0 ) === setImmediate( callback )
setTimeout(() => console.log('2 - setTimeout'), 0)

// process.nextTick( callback )
Promise.resolve('3 - promise').then(console.log)

console.log('4 - log 2 after timeout')

future = Date.now() + 5000

while( future > Date.now()) {  }

console.log('5 - log 3 - after while')


1 - log 1 before timeout
4 - log 2 after timeout
5 - log 3 - after while
3 - promise

/* IDLE - Thread finished, ready for new tasks */

0 - Keyboard event
2 - setTimeout
```

```js
setTimeout(()=>{
    console.log('open file')
    setTimeout(()=>{        
        console.log('file opened')
        console.log('read file')

        syncResult = window.prompt('Sync?')

        setTimeout((asyncResult) => {
            console.log('file read')
            console.log('send response')

            setTimeout(()=>{
                console.log('response sent')
                clearInterval(handle)
                console.log('end')
            },1000)    
        },1000)    
    },1000)
},1000)

handle = setInterval(() => console.log('do something else'), 300)

```

do something else
open file
do something else
file opened
read file
do something else
file read
send response
do something else
response sent
end

## setTimeout/setInterval, setImmediate, process.nextTick 
process.nextTick - before I/O

```js
// function someAsyncApiCall(callback) { callback() }
// function someAsyncApiCall(callback) { setTimeout(callback,0); }
// function someAsyncApiCall(callback) { setImmediate(callback); }
// function someAsyncApiCall(callback) { process.nextTick(callback); }
​
// placki
setTimeout(()=>console.log(2))
​
someAsyncApiCall(()=>console.log(3))
// placki
// placki
// placki
// placki
// placki
// placki
// placki
// placki
// placki
// placki
// placki
// placki
console.log(1)

```