Buffer.alloc(10)
<Buffer 00 00 00 00 00 00 00 00 00 00>

> b = Buffer.alloc(10)
<Buffer 00 00 00 00 00 00 00 00 00 00>

> buffer = Buffer.from('Ala ma kota')
<Buffer 41 6c 61 20 6d 61 20 6b 6f 74 61>

> buffer.toString()
'Ala ma kota'

> buffer.write('Bob')
3

> buffer.toString()
'Bob ma kota'

> buffer.write('psa',7)
3

> buffer.toString()
'Bob ma psaa'

> buffer.write('psa',7,11)
3

> b = Buffer.alloc(10)
<Buffer 00 00 00 00 00 00 00 00 00 00>

> b.write(null)
Uncaught TypeError: argument must be a string

    at Buffer.write (buffer.js:1045:17) {
  code: 'ERR_INVALID_ARG_TYPE'
}
> b.write('   ')
3

> b.toString()
'   \x00\x00\x00\x00\x00\x00\x00'

> b = Buffer.alloc(10,' ')
<Buffer 20 20 20 20 20 20 20 20 20 20>

> b.toString()
'          '

> buffer.toString()
'Bob ma psaa'

> buffer.fill(7)
<Buffer 07 07 07 07 07 07 07 07 07 07 07>

> buffer.write('Bob ma psaa')
11

> buffer.fill(' ',7)
<Buffer 42 6f 62 20 6d 61 20 20 20 20 20>

> buffer.toString()
'Bob ma     '

> buffer.write('psa',7)
3

> buffer.toString()
'Bob ma psa '

## Encoding

> buffer.toString('base64')
'Qm9iIG1hIHBzYSA='

> Buffer.from('Qm9iIG1hIHBzYSA=','base64')
<Buffer 42 6f 62 20 6d 61 20 70 73 61 20>

> Buffer.from('Qm9iIG1hIHBzYSA=','base64').toString()
'Bob ma psa '

> b = Buffer.from('Żółcią jaźń')
<Buffer c5 bb c3 b3 c5 82 63 69 c4 85 20 6a 61 c5 ba c5 84>
> b.length
17
> 'Żółcią jaźń'.length
11
> b = Buffer.from('Żółcią jaźń','ascii')
<Buffer 7b f3 42 63 69 05 20 6a 61 7a 44>
> b.toString()
'{�Bci\x05 jazD'
> b = Buffer.from('Żółcią jaźń','utf8')
<Buffer c5 bb c3 b3 c5 82 63 69 c4 85 20 6a 61 c5 ba c5 84>
> b.toString()
'Żółcią jaźń'
>


## Unsafe buffer

b = Buffer.alloc(10)
<Buffer 00 00 00 00 00 00 00 00 00 00>
> b = Buffer.allocUnsafe(10)
<Buffer ff ff ff ff 36 00 00 00 70 99>
> b.toString()
'����6\x00\x00\x00p�'


## BUffer slice
> buffer = Buffer.from('Ala ma kota')
<Buffer 41 6c 61 20 6d 61 20 6b 6f 74 61>

> osoba = buffer.slice(0,3)
<Buffer 41 6c 61>

> osoba.toString()
'Ala'

> zwierze = buffer.slice(7)
<Buffer 6b 6f 74 61>

> zwierze.toString()
'kota'

> osoba.write('Bob')
3

> zwierze.fill(' ').write('psa')
3

> buffer.toString()
'Bob ma psa '

> buffer.write('Cat')
3

> osoba.toString()
'Cat'
