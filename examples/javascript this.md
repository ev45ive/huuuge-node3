```js
function WhatIsThis(){
    console.log(this);
}

WhatIsThis()
VM10666:2 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

x = 123
123
window.x 
123
window.WhatIsThis
ƒ WhatIsThis(){
    console.log(this);
}

window.WhatIsThis()
VM10666:2 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

obj = { funkcja: WhatIsThis }
{funkcja: ƒ}
obj.funkcja()
VM10666:2 {funkcja: ƒ}

obj2 = {}
{}
obj2.func  = obj.funkcja
ƒ WhatIsThis(){
    console.log(this);
}
obj2.func()
VM10666:2 {func: ƒ}

obj.funkcja()
VM10666:2 {funkcja: ƒ}

window.WhatIsThis()
VM10666:2 Window {window: Window, self: Window, document: document, name: "", location: Location, …}

x = new WhatIsThis()
VM10666:2 WhatIsThis {}
WhatIsThis {}

x 
WhatIsThis {
    __proto__: constructor: ƒ WhatIsThis()
    __proto__: Object
}

x instanceof WhatIsThis
true


WhatIsThis.apply({x:123},[1,2,3])
VM10666:2 {x: 123}


WhatIsThis.call({x:123},1,2,3)
VM10666:2 {x: 123}



function Person(name){
    this.name = name;
}
```

```js

Person('Alice')


window.name 
"Alice"
obj = {}
Person.apply(obj,['Alice'])


obj 
{name: "Alice"}
obj = { __proto__: { constructor: Person }}

Person.apply(obj,['Alice'])


obj
Person {name: "Alice"}name: "Alice"__proto__: constructor: ƒ Person(name)__proto__: Object

obj instanceof Person
true 

obj = new Person('Bob')
Person {name: "Bob"}
name: "Bob"
    __proto__:
        constructor: ƒ Person(name)
        __proto__: Object

```
```js
function Person(name){
    this.name = name;
    this.sayHello = function(){
        return 'Hi I am ' + name
    }
}

alice = new Person('Alice')
bob = new Person('Bob')

console.log(alice.sayHello())
VM12711:10 Hi I am Alice

console.log(bob.sayHello())
VM12711:11 Hi I am Bob

alice.name = 'Tom'
"Tom"

alice.sayHello()
"Hi I am Alice" /// !!!
```

```js
function Person(name){
    this.name = name;
    this.sayHello = function(){
        return 'Hi I am ' + this.name
    }
}

alice = new Person('Alice')
bob = new Person('Bob')
console.log(alice.sayHello())
console.log(bob.sayHello())

alice.name = 'Tom'
"Tom"
alice.sayHello()
VM12928:10 Hi I am Alice
VM12928:11 Hi I am Bob
"Hi I am Tom"

button = { onclick:null }
{onclick: null}

button.onclick = alice.sayHello
ƒ (){
        return 'Hi I am ' + this.name
}
button.onclick() 
"Hi I am undefined"

button.name = 'button'
"button"

button.onclick() 
"Hi I am button"

button.onclick = alice.sayHello.bind(alice)
ƒ (){
        return 'Hi I am ' + this.name
    }
button.onclick() 
"Hi I am Tom"

function Person(name){
    this.name = name;
    this.sayHello = function(){
        return 'Hi I am ' + this.name
    }.bind(this) 
}

alice = new Person('Alice')

button = { onclick:null, name:'Button!' }
button.onclick = alice.sayHello
button.onclick()
"Hi I am Alice"


function Person(name){
    this.name = name;
    this.sayHello = () => {
        return 'Hi I am ' + this.name
    }
}
alice = new Person('Alice')

button = { onclick:null, name:'Button!' }
button.onclick = alice.sayHello
console.log(button.onclick())

alice.name = 'Tom'
alice.sayHello()

VM13618:12 Hi I am Alice
"Hi I am Tom"
```

```js
[1,2,3,4,5].map( (x) => { return x * 2 })
[1,2,3,4,5].map( (x) => x * 2 )
[1,2,3,4,5].map( x => x * 2 )

[1,2,3,4,5].map( x => {result:x+2} )
(5) [undefined, undefined, undefined, undefined, undefined]

[1,2,3,4,5].map( x => (  {result:x+2} )  )
(5) [{…}, {…}, {…}, {…}, {…}]


test = () => { console.log(this) }
() => { console.log(this) }
test.apply({x:123})
VM14058:1 Window {window: Window, self: Window, document: document, name: "Alice", location: Location, …}


new test()
VM14149:1 Uncaught TypeError: test is not a constructor
    at <anonymous>:1:1
```

```js
function Person(name){
    this.name = name;
    this.sayHello = function(){
        return 'Hi I am ' + this.name
    } 
}

alice = new Person('Alice')
bob = new Person('Bob')

Person {name: "Bob", sayHello: ƒ}
alice.sayHello === bob.sayHello
false

```

```js
function Person(name){
    this.name = name;
    this.sayHello = sayHello
}

function sayHello(){
    return 'Hi I am ' + this.name
} 

alice = new Person('Alice')
bob = new Person('Bob')


alice.sayHello === bob.sayHello
true
alice.sayHello()
"Hi I am Alice"
bob.sayHello()
"Hi I am Bob"

```


```js
function Person(name){
    this.name = name;
}
Person.prototype.sayHello = function(){
    return 'Hi I am ' + this.name
} 

alice = new Person('Alice')
bob = new Person('Bob')


alice.sayHello === bob.sayHello
true

alice.sayHello()
"Hi I am Alice"

bob.sayHello()
"Hi I am Bob"

alice 
Person {name: "Alice"}name: "Alice"__proto__: sayHello: ƒ ()constructor: ƒ Person(name)__proto__: Object

bob.sayHello = function(){ return 'I am not talking to you!' }


bob.sayHello()
"I am not talking to you!"

bob 
Person {
    name: "Bob",     
    sayHello: ƒ,
    prototype: { 
        constructor: ƒ Person(name) 
        sayHello: ƒ
        __proto__: Object
    }
bob.toString()
"[object Object]"
```

## Inheritance

```js
function Person(name){
    this.name = name;
}

Person.prototype.sayHello = function(){
    return 'Hi I am ' + this.name
} 

function Employee(name,salary){
    Person.apply(this, arguments)
    // Logger.apply(this)
    // OtherMixin...apply(this)
    this.salary = salary;
}

Employee.prototype = Object.create(Person.prototype)

Employee.prototype.work = function(){
    return 'Hi I need my ' + this.salary
} 

tom = new Employee('Tom',2500)

console.log(tom.sayHello())
VM15630:23 Hi I am Tom

console.log(tom.work())
VM15630:24 Hi I need my 2500

tom
Employee { 
    name: "Tom"
    salary: 2500
    __proto__: Person
        work: ƒ ()
        __proto__:
            sayHello: ƒ ()
            constructor: ƒ Person(name)

tom instanceof Employee
true

tom instanceof Person
true

tom instanceof Object
true

tom instanceof Array
false

```

## ES6 classes

```js
class Person{
    constructor(name){
        this.name = name
    }

    sayHello(){
        return 'My name is ' + this.name
    }
}

class Employee extends Person{
    constructor(name,salary){
        super(name)
        this.salary = salary
    }

    sayHello(){
        return super.sayHello() + ' and I am employee!'
    }

    work(){
        return ' I need my ' + this.salary
    }
}
tom = new Employee('Tom',1600)

```
Employee {
   name: "Tom"
   salary: 1600
   __proto__: Person
    constructor: class Employee
    sayHello: ƒ sayHello()
    work: ƒ work()
    __proto__: constructor: class Person
        sayHello: ƒ sayHello()
        __proto__: Object

```js
tom.sayHello()
"My name is Tom and I am employee!"

tom.work()
" I need my 1600"
```