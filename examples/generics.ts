

export { }


// const anyItem:any = 123
// anyItem.this.doesnt.exist.and.get.me.a.million.dollars()
// const item2 = anyItem.toUpperCase().randomFakeThing() + 123

function parse<T>(str: string): T { return {} as T }
function serialize<T>(obj: T) { return '' }

// const res1 = parse<{ name: string }>('{name:"tet"}')
const res1 = parse('{name:"tet"}') as { name: string }
// const res2 = serialize<{ name: string }>({ name: "tet" })
const res2 = serialize({ name: "tet" })


// function identity(item: any) { return item }
// function identity(item: string | number | boolean) { return item }
function identity<T>(item: T): T { return item }

let res3 = identity(123) // number
let res4 = identity('123') // string
let res5 = identity(true) // boolean

const res6 = identity(123) // liertal 123
const res7 = identity('123') // literal '234'
const res8 = identity(true) // literal true

const arr1: Array<number> = [1, 2, 3]
const arr2: Array<string> = ['1', '2', '3']
const arr3: Array<string | number> = ['1', '2', '3', 123]

function getFirst<T>(arr: T[]): T {
  return arr[0]
}

const item1/* :number */ = getFirst([1, 2, 3])
item1.toFixed()

const item2 = getFirst(['1', '2', '3'])
// const item3 = getFirst<string>(['1', '2', '3', 123]) // Error
const item3 = getFirst(['1', '2', '3', 123])
// const item4 = getFirst<string>(/* arr: string[] */)
const item4 = getFirst([1, 2, 3]) // number
// const item5:string = getFirst(/* arr: string[] */)


function getId<T extends { id: string }>(item: T): T['id'] {
  return item.id
}
// getId({ id: 123, type: 'user' }) // Error
getId({ id: '123', type: 'user' })
getId({ id: '123', type: 'product' })


interface Product {
  id: string,
  name: string,
  price: number,
  categories: [],
  createdAt: Date
  brand: string,
}
// interface ProductCreateDTO {
//   name: string, price: string, categories?: []
// }
// interface ProductUpdateDTO {
//   id: string, name?: string, price?: string, categories?: []
// }
// interface ProductCreateDTO {
//   name: Product['name'],
//   price?: Product['price'],
//   categories?: Product['categories']
//   // brand?: Product['brand']
// }

type Dictionary = {
  [myKey: string]: string
}

const dict: Dictionary = {
  '123': '123',
  234: '123',
  // '345': 213, // Type 'number' is not assignable to type 'string'.
}

// type ProductKeys = 'id' | 'name' | 'price'
type ProductKeys = keyof Product
const key: ProductKeys = 'price'


type PartialProduct = {
  // [myKey in ProductKeys]: string
  // [myKey in ProductKeys]: Product['id']
  // [myKey in ProductKeys]: Product[myKey]
  [key in ProductKeys]?: Product[key]
}
const payload: PartialProduct = {
  name: '123', price: 0
}

// type Partial<T, TKeys extends keyof T = keyof T> = {
//   [key in TKeys]?: T[key]
// }
// type ProductCreateDTO = Partial<Product, 'name' | 'price' | 'awd'> // Error
// type ProductCreateDTO = Partial<Product> //, 'name' | 'price'>
// type ProductCreateDTO = Partial<Product, 'name' | 'price'>
// type ProductCreateDTO = Partial<Product, 'name' | 'price'> & { requestId: string }

type ProductCreateDTO = Readonly<Partial<Product> & { requestId: string }>

const dto: ProductCreateDTO = {
  // name: 123, // Error
  name: '123',
  requestId: '123'
}

const snapshot = Object.freeze({ // Readonly<{...}> 
  // name: 123, // Error
  name: '123',
  requestId: '123', options: { x: 123 }
})

type Option = 'ACTIVE' | 'PENDING' | 'INACTIVE'// | 'ERROR'

function handleOption(option: Option) {
  if (option == 'ACTIVE') {
    return ''
  } else if (option == 'INACTIVE') {
    return ''
  } else if (option == 'PENDING') { return '' }
  else {
    // const _neverHappens: never = option
    // throw new Error('Unexpected option ' + option)
    checkExhaustiveness(option)
  }
}

function checkExhaustiveness(option: never): never {
  throw new Error('Unexpected option ' + option)
}

type OptionError = Option | 'ERROR'
const o: OptionError = 'ERROR'

type AB = Exclude<'A' | 'B' | 'C', 'C'>
type ABC = Extract<'A' | 'B' | 'C' | 123, string>

function abc(a: string, b: number, c: boolean) { return { name: a } }

type abcType = typeof abc;
type abcParams = Parameters<typeof abc>
type abcReturn = ReturnType<typeof abc>

type EntityType<T extends { type: string }> = T extends { type: infer P } ? P : never;

type E = EntityType<{ type: 'product' } | { type: 'user' }>