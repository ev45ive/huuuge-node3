# GIT
cd ..
git clone https://bitbucket.org/ev45ive/huuuge-node3.git huuuge-node3
cd huuuge-node3
npm i 

npm start

# Git pull update
git stash -u
git pull -f 

npm start

# Instalacje
node -v 
v14.16.1
https://nodejs.org/en/
yarn --version
1.13.0

npm -v
6.14.6

code -v 
1.56.2
https://code.visualstudio.com/

git --version
git version 2.31.1.windows.1
https://git-scm.com/download/win

chrome://version/ 
90

docker --version
Docker version 20.10.6, build 370c289

docker run -d -p 80:80 docker/getting-started
http://localhost:80/

## Update nodejs

https://phoenixnap.com/kb/update-node-js-version

npm i -g n
npm ERR! code EBADPLATFORM
npm ERR! notsup Unsupported platform for n@7.2.2: wanted {"os":"!win32","arch":"any"} (current: {"os":"win32","arch":"x64"})

## Node Version Manager
https://tecadmin.net/install-nodejs-with-nvm/


## Npm init
npm init -y
Wrote to C:\Projects\szkolenia\huuuge-node-3\package.json:

yarn add express
npm install express
npm i express
npm i express@4.10

<!-- install from package.json and update respecting semver -->
npm i 
<!-- install from package-lock.json and update exact version -->
npm ci 

# Semver
https://semver.org/
https://semver.npmjs.com/
https://www.npmjs.com/package/semver
https://docs.npmjs.com/about-semantic-versioning

## Update
npm audit
npm outdated
Package  Current  Wanted  Latest  Location
express   4.10.1  4.10.8  4.17.1  huuuge-node-3

<!-- update respecting semver - and update semver -->
npm update 
<!-- update to lastest and update semver to lastest -->
npm install packagexyz
<!-- update to concrete and update semver -->
npm install packagexyz@^4

## Global
npm i -g typescript
C:\Users\<USERNAME>\AppData\Roaming\npm\tsc -> C:\Users\<USERNAME>\AppData\Roaming\npm\node_modules\typescript\bin\tsc
echo $PATH

tsc --version
Version 4.3.2

## CommonJS modules vs Es6 modules
(node:10432) Warning: To load an ES module, set "type": "module" in the package.json or use the .mjs extension.
(Use `node --trace-warnings ...` to show where the warning was created)

<!-- ES6 Static module structure -->
file:///C:/Projects/szkolenia/huuuge-node-3/examples/index.js:2
import { getPlacki, getTitle, secret } from './lib/mylib.js';
                              ^^^^^^
SyntaxError: The requested module './lib/mylib.js' does not provide an export named 'secret'

## Typescript
npm i -g typescript

tsc examples/typescript.ts 
tsc --watch examples/typescript.ts 

tsc --target ES2020 examples/typescript.ts --module es2020 --strict

## Tsconfig.json
tsc --target ES2020 --module es2020 --strict --init
<!-- Create tsconfig.json -->
tsc --init


## Type Declarations
Cannot find name 'process'. Do you need to install type definitions for node? Try `npm i --save-dev @types/node`.ts(2580)

npm i --save-dev @types/node

## Express
npm i express
npm i --save-dev @types/express

## Nodemon
tsc --watch
nodemon dist

npm run build:watch
npm run start:watch

"scripts":{
    "watch": " nodemon --watch \"src/**\" --ext \"ts\" --exec \"node -r ts-node/register ./src/index.ts \" ",

    
  "nodemonConfig": {
    "watch": "src/**",
    "ext": "ts",
    "exec": "node -r ts-node/register ./src/index.ts"
  },

## Debugger
node --inspect dist/index.js 

Debugger listening on ws://127.0.0.1:9229/23e3b033-4fc9-44e5-a0ae-ed2501f108fd
For help, see: https://nodejs.org/en/docs/inspector


## Sourcemaps
tsconfig:
 "sourceMap": true,                           /* Generates corresponding '.map' file. */
//  "inlineSourceMap": true,                     /* Emit a single file with source maps instead of having a separate file. */
// "inlineSources": true,                       /* Emit the source alongside the sourcemaps within a single file; requires '--inlineSourceMap' or '--sourceMap' to be set. */

node --enable-source-maps ./dist/index.js
Error: error
    at C:\Projects\szkolenia\huuuge-node-3\dist\index.js:9:11
        -> C:\Projects\szkolenia\huuuge-node-3\src\index.ts:7:11

node --inspect --enable-source-maps ./dist/index.js
node --inspect-brk --enable-source-maps ./dist/index.js

"scripts":{
    "watch:debug": "nodemon --exec \"node --inspect --enable-source-maps -r ts-node/register ./src/index.ts\"",

debugger;

## Process Environment
process.exit(0) vs process.exit(errorCode)

https://12factor.net/config

https://www.npmjs.com/package/cross-env
cross-env PORT=8000 node script.js
process.env.PORT === 8080

https://www.npmjs.com/package/dotenv
echo 'PORT=8000' > .env
node --require dotenv/config script.js
process.env.PORT === 8080

## Chrome memory
https://chromium.googlesource.com/chromium/src.git/+/refs/heads/main/docs/memory/key_concepts.md

book = 'Pan Tadeusz'
book += '!' // Copy!

## Buffer
https://nodejs.org/dist/latest-v14.x/docs/api/buffer.html


## Jest 
https://jestjs.io/docs/cli
https://kulshekhar.github.io/ts-jest/

npm i -D jest typescript ts-jest @types/jest

npx ts-jest config:init

npx jest

// package.json
"scripts": { "test": "jest" }

## supertest http
https://www.npmjs.com/package/supertest

npm i -D supertest @types/supertest


## Error handling
https://www.npmjs.com/package/express-promise-router

npm install express-promise-router --save
